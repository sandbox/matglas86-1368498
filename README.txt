== Description ==
Adds a permissions model onto relationships that can be used to grant CRUD 
permissions for nodes for a specific user by role or specifically for them. 
These permissions are for the child nodes of the node a user has a Relation 
with Permissions.

== Example ==

User <-> Event <-> Photo
               <-> ... (any other

                ^ Is media from
      ^ Is attending

== Example explained ==
We might have a 'Event', for example, which people can attend. We have a 
organizer (User(s)) and and attenders (Users). The relation between them and 
the event is 'is attending'.

All attending people will be able to add new 'Photo'(s) to the 'Event' when 
they are attending the 'Event'. We create a relation type 'is media from' to 
relate photo's (or anything else) to a 'Event'.

We add the Relation Permissions field to the relation type 'is
attending'.

Based on the relation User <-> Event we know that there can be permissions to 
add Photos to the event. The Relation Permissions gathers this information and 
allows to define role permissions per relation type. On a per relation basis 
(User <-> Event) a role can be assigned. Or it is possible to override the role 
and assign specific permissions for the relation.

== Tasks to complete ==
- Manage roles per Relation Type (70%)
- Node Access (0%)
- Manage relation Roles (0%)
- Manage permission overrides (0%)
- Entity Access (0%)

== Dependecies ==
- Relation
- CTools (dev) !IMPORTANT
- Entity

Don't use on PRODUCTION sites. There will be no support when things go wrong.