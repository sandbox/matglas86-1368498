<?php
function relation_perms_page_types($type_name = NULL) {
  $relation_entity_info = entity_get_info('relation');
  $relation_bundles = isset($relation_entity_info['bundles']) ? $relation_entity_info['bundles'] : array();

  // Show the relation types as a table.
  if ($type_name == NULL) {
    $table = array(
      'header' => array(
        t('Relation types'),
        t('Actions'),
      ),
      'rows' => array(),
      'attributes' => array(),
      'caption' => NULL,
      'sticky' => FALSE,
      'empty' => t('There are no User <-> Entity relations with permissions field set on it.'),
      'colgroups' => array()
    );

    $relation_types = relation_perms_get_relation_types_with_perms_field();
    foreach ($relation_types as $type => $value) {
      $type = $relation_bundles[$type];

      // Relation name.
      $row = array();
      $row[] = t(ucfirst($type['label'])); // @todo Add link to edit type if user_acces correct.
      $row[] = l(t('Roles'), 'admin/people/relation-perms/' . $type['relation_type']);

      $table['rows'][] = $row;
    }
    return theme_table($table);
  } else {

    if (array_key_exists($type_name, $relation_bundles)) {
      $relation_type = $relation_bundles[$type_name];

      $elements = array();

      $add_role = array(
        array('#markup' => '<p>'),
        array(
          '#theme' => 'link',
          '#text' => t('Add role'),
          '#path' => 'admin/people/relation-perms/'. $type_name . '/add/role',
          '#options' => array('attributes'=> array(), 'html' => FALSE),
        ),
        array('#markup' => '</p>'),
      );
      $elements[] = $add_role;

      $table = array(
        '#theme' => 'table',
        '#header' => array(
          t('Roles'),
          t('Actions'),
        ),
        '#rows' => array(),
        '#attributes' => array(),
        '#caption' => NULL,
        '#sticky' => FALSE,
        '#empty' => t('There are no roles yet for this relation type.'),
        '#colgroups' => array()
      );
      $elements['table'] = $table;

      $query = db_select('relation_permissions_roles', 'rpr');
      $query->addField('rpr', 'role_id');
      $query->addField('rpr', 'name');
      $query->addField('rpr', 'settings');
      $query->condition('relation_type', $type_name, '=');

      $result = $query->execute();

      foreach($result->fetchAll() as $role) {
        $role_settings = unserialize($role->settings);

        $elements['table']['#rows'][] = array(
          $role_settings['name'],
          l('Edit', 'admin/people/relation-perms/' . $type_name .  '/edit/' . $role->role_id)
        );
      }

      return render($elements);
    } else {
      return t('This relation type does not exist.');
    }
  }
  return '';
}

/**
 * Return the title for the page roles.
 */
function relation_perms_page_types_title($arg, $prefix = '', $suffix = '') {
  if (isset($arg) && is_string($arg)) {
    $type_info = relation_perms_get_relation_type_info($arg);
    return trim(t($prefix .  " '" . $type_info['label'] . "' " . $suffix));
  }
  return NULL;
}

/**
 * Return the title for the role edit page.
 */
function relation_perms_page_role_title($arg, $prefix = '', $suffix = '') {
  if (isset($arg) && (is_string($arg) || is_numeric($arg))) {
    $role = relation_perms_get_role($arg);
    if (isset($role['settings']['name'])) {
      return trim(t($prefix .  " '" . $role['settings']['name'] . "' " . $suffix));
    }
  }
  return NULL;
}

/**
 * Return a page with a add role form including its permissions.
 *
 * @todo Style this grid with a table.
 *   See: user.module, user.admin.inc, theme_user_admin_permissions()
 *   and user_admin_permissions().
 *
 * @todo Add delete action.
 *
 * @todo Add alter function for permission label generation.
 *   This might be valuable for custom implementations where
 *   the client wants better labels or add a description.
 */
function relation_perms_page_type_role_edit($form, &$form_state, $type_name, $data = NULL) {
  $base_perms = relation_perms_get_relation_perms_base($type_name);
  if (isset($base_perms)) {

    // Set default values then load role object.
    $role_obj = array(
      'name' => '',
      'settings' => array(
        'name' => ''
      ),
    );
    if (isset($data)) {
      $role_obj = relation_perms_get_role($data);
    }

    //$form['#tree'] = TRUE;
    $form['name'] = array(
      '#title' => t('Role name'),
      '#type' => 'textfield',
      '#default_value' => $role_obj['settings']['name'],
      '#description' => t('The human-readable name of this role. This text will be displayed as part of the role list. It is recommended that this name begin with a capital letter and contain only letters, numbers, and spaces. This name must be unique.'),
      '#required' => TRUE,
      '#size' => 32,
    );
    $form['machine_name'] = array(
      '#type' => 'machine_name',
      '#max_length' => 32,
      '#default_value' => $role_obj['name'],
      '#disabled' => isset($data),
      '#machine_name' => array(
        'exists' => 'relation_perms_role_name_exists',
      )
    );

    // Formatting array for perm labels in the bundle forms.
    $perm_format = array(
      'create' => 'Can create <i>!type</i>',
      'read:own' => 'Can view own <i>!type</i>',
      'read:any' => 'Can view any <i>!type</i>',
      'update:own' => 'Can edit own <i>!type</i>',
      'update:any' => 'Can edit any <i>!type</i>',
      'delete:own' => 'Can delete own <i>!type</i>',
      'delete:any' => 'Can delete any <i>!type</i>',
    );

    // Create permission elements per bundle.
    $bundle_names = $base_perms['to'];
    foreach ($bundle_names as $i => $to) {
      // Get bundle information
      $entity_name = substr($to, 0, strpos($to, ':'));
      $bundle_name = substr($to, strpos($to, ':') + 1);
      $bundle = _relation_perms_bundle_info($entity_name, $bundle_name);

      // Create a form for a bundle.
      $bundle_form = array();
      $bundle_form['title'] = array(
        '#markup' => '<p><b>' . t($bundle['label']) . '</b></p>',
      );

      foreach ($base_perms['perms'] as $perm_key => $perm_value) {
        // Only do this for the current bundle.
        if (strpos($perm_key, $to) === 0) {

          $perm_key_str = str_replace(':', '_', $perm_key);
          $perm_part = str_replace($to . ':', '', $perm_key);

          $default_value = 'ignore';
          if (isset($role_obj['permissions'][$perm_key])) {
           $default_value = $role_obj['permissions'][$perm_key];
          }

          $bundle_title = t($bundle['label']);
          $bundle_form['perm_' . $perm_key_str]['title'] = array('#markup' => '<p><b>' . t($perm_format[$perm_part], array('!type' => $bundle_title)) . '</b></p>');
          $bundle_form['perm_' . $perm_key_str] += array(
            'ignore' => array(
              '#type' => 'radio',
              '#return_value' => 'ignore',
              '#default_value' => $default_value,
              '#title' => 'Ignore',
              '#parents' => array('perm_' . $perm_key_str),
            ),
            'allow' => array(
              '#type' => 'radio',
              '#return_value' => 'allow',
              '#default_value' => $default_value,
              '#title' => 'Allow',
              '#parents' => array('perm_' . $perm_key_str),
            ),
            'deny' =>array(
              '#type' => 'radio',
              '#return_value' => 'deny',
              '#default_value' => $default_value,
              '#title' => 'Deny',
              '#parents' => array('perm_' . $perm_key_str),
            ),
          );
          unset($base_perms['perms'][$perm_key]);
        }
      }
      $form[$to] = $bundle_form;
    }

    // Add actions.
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Submit'));

    return $form;
  }

  drupal_not_found();
  return;
}

/**
 * Retrieve a perm value.
 */
function _relation_perms_map_perm_keys($values, $perm_key) {
  $perm_key_str = 'perm_' . str_replace(':', '_', $perm_key);
  if (array_key_exists($perm_key_str, $values)) {
    return $values[$perm_key_str];
  }
}

/**
 * Get information about a bundle.
 *
 * @param string $entity_name
 * @param string $bundle_name
 */
function _relation_perms_bundle_info($entity_name, $bundle_name) {
  if (isset($entity_name) && isset($bundle_name)) {
    $entity = entity_get_info($entity_name);

    if (isset($entity['bundles'][$bundle_name])) {
      return $entity['bundles'][$bundle_name];
    }
  }
  return NULL;
}

function relation_perms_page_type_role_edit_submit($form, &$form_state) {
  $path_relation_type = arg(3);
  $path_role_id = arg(5);

  $role_obj = relation_perms_get_role($path_role_id);
  $values = $form_state['values'];
  $perm_base = relation_perms_get_relation_perms_base($path_relation_type);

  if (isset($perm_base)) {
    foreach($perm_base['perms'] as $perm_key => &$value) {
      $new_perm_value = _relation_perms_map_perm_keys($values, $perm_key);
      $value = ($new_perm_value === RELATION_PERMS_ALLOW ||
                $new_perm_value === RELATION_PERMS_DENY ? $new_perm_value : NULL);
    }

    $role = array(
      'relation_type' => $path_relation_type,
      'name' => $values['machine_name'],
      'settings' => serialize(
        array('name' => $values['name'])
      ),
      'permissions' => serialize($perm_base['perms'])
    );
    if (isset($role_obj)) {
      $role['role_id'] = $role_obj['role_id'];
    }

    // Role id is added when this comes from a role edit submition.
    // When no role id was added it creates a new record.
    $record_primary = (isset($role_obj) && !empty($role_obj) ? 'role_id' : array());
    $response = drupal_write_record('relation_permissions_roles', $role, $record_primary);
    if ($response == SAVED_UPDATED) {
      drupal_set_message(t('Updated role !rolename', array('!rolename' => t($values['name']))));
    }
    elseif ($response == SAVED_NEW) {
      drupal_set_message(t('Created role !rolename', array('!rolename' => t($values['name']))));
    }
    drupal_goto('admin/people/relation-perms/' . $path_relation_type);
  }
}

/**
 * Return a confirmation page for role detele.
 */
function relation_perms_page_type_role_delete($form, &$form_state, $type_name, $data = NULL) {
  return NULL;
}
