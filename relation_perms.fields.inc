<?php

/**
 * Implements hook_field_info().
 *
 * @see hook_field_info
 */
function relation_perms_field_info() {
  return array(
    RELATION_PERMS_FIELD_TYPE => array(
      'label' => t('Relation Permissions'),
      'description' => t('Provides CRUD permissions for creating nodes with relations based on the permissions in the relation.'),
      'default_widget' => 'relation_perms_check_permissions',
      'default_formatter' => 'relation_perms_show_permissions',
      'entity_tyes' => array('relation'),
      'instance_settings' => array(
        'roles' => array(),
        'override' => FALSE,
        'permissions' => array(),
      ),
    )
  );
}

/**
 * Implements hook_field_is_empty().
 *
 * @see hook_field_is_empty
 */
function relation_perms_field_is_empty($item, $field) {
  $empty = FALSE;
  if (!isset($item['roles']) || (isset($item['roles']) && !is_array($item['roles']))) {
    $empty = TRUE;
  }
  return $empty;
}



/**
 * Implements hook_field_formatter_info()
 *
 * @see hook_field_formatter_info
 */
function relation_perms_field_formatter_info() {
  return array(
    'relation_perms_show_roles' => array(
      'label' => t('Show the assigned roles for this user'),
      'field types' => array(RELATION_PERMS_FIELD_TYPE),
    )
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function relation_perms_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'relation_perms_show_permissions':
      break;
  }

  return $element;
}

/**
 * Implements hook_field_widget_info()
 */
function relation_perms_field_widget_info() {
  return array(
    'relation_perms_check_permissions' => array(
      'label' => t('Permission checkboxes'),
      'field types' => array(RELATION_PERMS_FIELD_TYPE),
    )
  );
}

/**
 * Implements hook_field_widget_form()
 *
 * @see hook_field_widget_form
 */
function relation_perms_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  _relation_perms_field_item_unserialize($items);
  $value = $instance['settings'];
  if (isset($instance['default_value'][$delta])) {
    $value = $instance['default_value'][$delta];
  }
  if (isset($items[$delta])) {
    $value = $items[$delta];
  }

  // Get the element and set the delta as reference.
  $widget_type = $instance['widget']['type'];
  $widget_base = array();
  $widget_base['#delta'] = $delta;

  if ($widget_type == 'relation_perms_check_permissions') {
    $roles = relation_perms_get_roles($element['#bundle']);

    // Create role widget.
    $widget_roles = $widget_base;
    $widget_roles += array(
      '#type' => 'checkboxes',
      '#title' => t('Roles'),
      '#default_value' => is_array($value['roles']) ? $value['roles'] : array(),
    );
    foreach ($roles as $role) {
      $widget_roles['#options'][$role['name']] = $role['settings']['name'];
    }
    $element['roles'] = $widget_roles;

    // The element for override and permissions are partially here
    // this feature might be added in the future.
    /*

    // Create override widget.
    $widget_override = $widget_base;
    $widget_override = array(
      '#type' => 'checkbox',
      '#title' => t('Override the permissions'),
      '#default_value' => isset($value) ? $value['override'] : NULL,
    );
    $element['override'] = $widget_override;

    // Create override widget.
    $widget_permissions = $widget_base;
    $widget_permissions = array(
      '#type' => 'checkboxes',
      '#title' => t('Permission grants'),
      '#default_value' => isset($value) ? $value['permissions'] : NULL,
    );
    $widget_permissions['#options'] = array('node_page_create' => 'Node create');
    $element['permissions'] = $widget_permissions;

		*/
  }
  return $element;
}

/**
 * Implements hook_field_presave().
 */
function relation_perms_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  if ($field['type'] == RELATION_PERMS_FIELD_TYPE) {
    if (count($items) == 0) {
      $items[] = $instance['default_value'][0];
    }

    foreach ($items as &$item) {
      _relation_perms_field_item_serialize($item);
    }
  }
}
function relation_perms_field_load($entity_type, $entities, $field, $instances, $langcode, &$items, $age) {
  foreach ($items as &$item) {
    _relation_perms_field_item_unserialize($item);
  }
}
function relation_perms_field_update($entity_type, $entity, $field, $instance, $langcode, &$items) {
  dpm($items, 'update');
  foreach ($items as &$item) {
    _relation_perms_field_item_serialize($item);
  }
  dpm($items, 'update');
}

/**
 * Unserialize the blob columns.
 *
 * You can pass a single item or more.
 */
function _relation_perms_field_item_unserialize(&$items) {
  // Unserialize one item.
  if (isset($items['roles'])) {
    $items['roles'] = is_string($items['roles']) ? unserialize($items['roles']) : $items['roles'];
  }
  elseif (isset($items) && is_array($items) && is_numeric(key($items)) &&
          isset($items[key($items)]['roles']))
  {
   foreach ($items as &$item) {
    $item['roles'] = is_string($item['roles']) ? unserialize($item['roles']) : $item['roles'];
   }
  }
}

/**
 * Serialize the blob columns to save them.
 *
 * You can pass a single item or more.
 */
function _relation_perms_field_item_serialize(&$items) {
  // Serialize one item.
  if (isset($items['roles'])) {
    $items['roles'] = serialize($items['roles']);
  }
  elseif (isset($items) && is_array($items) && is_numeric(key($items)) &&
          isset($items[key($items)]['roles']))
  {
   foreach ($items as &$item) {
    $item['roles'] = serialize($item['roles']);
   }
  }
}
